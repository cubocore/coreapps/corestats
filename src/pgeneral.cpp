/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#include <QStringListModel>
#include <QScreen>
#include <QDebug>
#include <QDir>
#include <QStorageInfo>

#include <cprime/cprime.h>
#include <csys/systeminfo.h>
#include <csys/cpuinfo.h>

#include "global.h"
#include "pgeneral.h"
#include "ui_pgeneral.h"


pgeneral::pgeneral(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::pgeneral)
    , timer(new QBasicTimer())
{
	ui->setupUi(this);

    timer->start(1000, this);

    updateInfo();
	systemInformationInit();
}

pgeneral::~pgeneral()
{
	delete timer;
    delete ui;
}

void pgeneral::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == timer->timerId()) {
        updateInfo();
    }
}

void pgeneral::updateInfo()
{
	// initialization
	updateCpuBar();
	updateMemoryBar();
    updateNetworkBar();
    updateStorageBar();
}

void pgeneral::systemInformationInit()
{
    QSize scSize = qApp->primaryScreen()->size();
    QString scr = QString("%1 x %2").arg(scSize.width()).arg(scSize.height());
    QString formfactor;


    if (CPrime::ThemeFunc::getFormFactor() == CPrime::FormFactor::Mobile) {
        formfactor = "Mobile";
    } else if (CPrime::ThemeFunc::getFormFactor() == CPrime::FormFactor::Tablet &&  CPrime::ThemeFunc::getTouchMode() == true) {
        formfactor = "Tablet";
    } else {
        formfactor = "Desktop";
    }

	// get system information
    QSysInfo sys;
    SystemInfo sysInfo;
    QStorageInfo storage = QStorageInfo::root();

	QStringList left;
    left << "User Name" << "Platform" << "Host" << "Kernel Release" << "Root File System" << "Form Factor"
		 << "CPU Model" << "CPU Architecture" << "CPU Speed" << "CPU Cores" << "RAM Size" << "Primary Display Size"
         << "Bootup time" << "Uptime" << "Qt Toolkit Version";

	QStringList right;
    right << sysInfo.getUsername() << sysInfo.getPlatform() << sysInfo.getDistribution()
          << sysInfo.getKernel() << storage.fileSystemType() << formfactor << sysInfo.getCpuModel() << sys.currentCpuArchitecture() << sysInfo.getCpuSpeed()
          << sysInfo.getCpuCore() << CPrime::FileUtils::formatSize(infoMgr->getMemTotal()) << scr << sysInfo.getBootTime() << sysInfo.getUptime() << QT_VERSION_STR;

    ui->infoLeft->clear();
    ui->infoRight->clear();

    int count = left.count();
    QString sep = "\n";

    for (int i = 0; i < count; i++) {
		if (i + 1 == count) {
			sep = "";
		}

        ui->infoLeft->setText(ui->infoLeft->text() + left[i] + sep);
        ui->infoRight->setText(ui->infoRight->text() + ": " + right[i] + sep);
	}
}

void pgeneral::updateCpuBar()
{
	int cpuCount = infoMgr->getCpuCoreCount();
    int cpuUsedPercent = 0;

    QList<QList<double>> cpuPercents( infoMgr->getCpuPercents(1) );
	Q_FOREACH (QList<double> pc, cpuPercents) {
        cpuUsedPercent += pc[ 0 ];
    }

    cpuUsedPercent = static_cast<int>(cpuUsedPercent / cpuCount);

	ui->cpu->setText(QString::number(cpuUsedPercent) + "%" + "<sup>CPU</sup>");
}

void pgeneral::updateMemoryBar()
{
    int memUsedPercent = 0;

    QList<double> memUsage( infoMgr->getMemUsed(1) );
    if (infoMgr->getMemTotal()) {
        memUsedPercent = static_cast<int>((memUsage[ 0 ] / static_cast<double>(infoMgr->getMemTotal())) * 100.0);
    }

    ui->ram->setText(QString::number(memUsedPercent) + "%" + "<sup>RAM</sup>");
}

void pgeneral::updateNetworkBar()
{
    QList<QList<double>> IOSpeed = infoMgr->getNetworkSpeed(1);

    ui->net->setText("<p><sub>NET   </sub>"+ CPrime::FileUtils::formatSize(IOSpeed[ 1 ][ 0 ]) +"<sup>UP</sup></p><p>   "+CPrime::FileUtils::formatSize(IOSpeed[ 0 ][ 0 ])+"<sub>DN</sub></p>");
}

void pgeneral::updateStorageBar()
{
    QStorageInfo s(QDir::root());

    qint64 total = s.bytesTotal();
	qint64 free = s.bytesAvailable();

    qint64 used = total - free;

    int percent = (used / static_cast<double>(total)) * 100;
    ui->storage->setText(QString::number(percent) + "%" + "<sup>DISK</sup>");

	qint64 reserved = s.bytesFree() - s.bytesAvailable();
	if (reserved > 0) {
//		ui->storage->setText(ui->storage->text() + "<br/><sub><font size='0.1em'>Reserved: " + CPrime::FileUtils::formatSize(reserved) + "</font></sub>");
	}
}
