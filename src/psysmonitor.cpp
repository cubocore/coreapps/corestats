/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QLabel>
#include <QDebug>

#include <sensors/sensors.h>
#include <sensors/error.h>

#include "global.h"
#include "psysmonitor.h"
#include "ui_psysmonitor.h"

#include <csys/cpuinfo.h>


pSysMonitor::pSysMonitor(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::pSysMonitor)
{
    ui->setupUi(this);

    timer = new QBasicTimer;
    timer->start(2000, this); // Update in every 2 seconds

    startSetup();
}

pSysMonitor::~pSysMonitor()
{
	delete timer;
    delete ui;
}

void pSysMonitor::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == timer->timerId()) {
        updateFreqs();
        updateSensors();
    }
}

void pSysMonitor::startSetup()
{
	updateFreqs();
    updateSensors();

    connect(ui->refreshSensor, &QToolButton::clicked, this, &pSysMonitor::updateSensors);
}

QStringList pSysMonitor::sensors()
{
    QStringList dataList;
    int chipNumber, temp;
	const sensors_chip_name *chip;

    sensors_cleanup();

    if (int err = sensors_init(nullptr)) {
        qWarning() << "Error occurred while collecting sensor data" << sensors_strerror(err);
        return QStringList();
    } else {
        chipNumber = 0;
		char buf[256] = {0};
		const sensors_subfeature *sub;

        while ((chip = sensors_get_detected_chips(nullptr, &chipNumber))) {
            QString data = "";

            if (sensors_snprintf_chip_name(buf, 256, chip) < 0) {
                snprintf(buf, 256, "%i", chipNumber);
            }

            data += "Chip Name: " + QString::fromLocal8Bit(buf) + "\n";

			const char *adap = sensors_get_adapter_name(&chip->bus);
            if (adap) {
                data += "Adapter Name: " + QString::fromLocal8Bit(adap) + "\n";
            }

            temp = 0;
			const sensors_feature *feature;
            while ((feature = sensors_get_features(chip, &temp))) {
                char *str = sensors_get_label(chip, feature);
                if ( str == nullptr ) {
                    continue;
                }

                double val = 0.0;
                sub = sensors_get_subfeature(chip, feature, (sensors_subfeature_type)(((int)feature->type) << 8));
                if ( sub == nullptr ) {
                    continue;
                }

                sensors_get_value(chip, sub->number, &val);
                data += QString::fromLocal8Bit(str) + ": " + QString::number(val) + '\n';

                free(str);
            }

            dataList.append(data);
        }
    }

    return dataList;
}

void pSysMonitor::updateFreqs()
{
	QStringList cpuFreqs = infoMgr->getCpuFrequenciesStr();

    int count = ui->cpuFreqsLayout->count();

    while (count--) {
		auto layoutItem = ui->cpuFreqsLayout->takeAt(count);
		delete layoutItem->widget();
		delete layoutItem;
    }

	int index = 0;
	int maxCol = 4;

    for (const auto& freq : cpuFreqs) {
		auto lbl = new QLabel(freq, this);
		lbl->setWordWrap(true);
		ui->cpuFreqsLayout->addWidget(lbl, (index / maxCol), (index % maxCol));
		index++;
    }
}

void pSysMonitor::updateSensors()
{
    QStringList sensorsData = sensors();

    int count = ui->sensorsLayout->count();
    while (count--) {
        auto layoutItem = ui->sensorsLayout->takeAt(count);
        delete layoutItem->widget();
        delete layoutItem;
	}

	int index = 0;
	int maxCol = 2;

    for (const auto& sensor : sensorsData) {
        auto lbl = new QLabel(sensor, this);
		lbl->setWordWrap(true);
		ui->sensorsLayout->addWidget(lbl, (index / maxCol), (index % maxCol));
		index++;
    }
}
