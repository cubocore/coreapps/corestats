/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QLabel>
#include <QToolButton>
#include <QLayout>
#include <QPainter>
#include <QPainterPath>
#include <QPaintEvent>
#include <QDebug>

#include "global.h"
#include "usagegraph.h"
#include <cprime/filefunc.h>

QList<QColor> colors = {
	0xFF0000, 0x00FF00, 0x0088FF, 0xFFFF00, 0xFF00FF, 0x00FFFF,
	0xBB9900, 0xFAAAAA, 0xFF0088, 0xBBFF99, 0xFF8800, 0xAAAAFF,
	0x8EBB00, 0x0BBB9F, 0x8F0FFF, 0x9988AA, 0xAA6050, 0xA63000,
	0xD166FF, 0x00A4D4, 0x0ACCCC, 0xFF5F9F, 0xDFDFBF, 0xBFFFFF,
};

static QColor getColorFor(int index) {
	int i = index % colors.size();
	return colors[i];
}

/**
 * UsagePlot - Draw the graph for resource usages
 */

UsagePlot::UsagePlot(int resources, QWidget *parent) : QWidget(parent)
{
    /* Init values for resources */
    mResources = resources;

    for (int r = 0; r < resources; r++) {
        UsageValues values;
        values << 0;                    // First value is always 0
        mResValMap[ r ] = values;
    }

    /* SizePolicy */
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
}

void UsagePlot::setYRange(double min, double max)
{
    minY = min;
    maxY = max;

    repaint();
}

void UsagePlot::updateValues(const QList<UsageValues> &values, int resources)
{
	if (mResources < resources) {
		mResources = resources;
	}

    for( int i = 0; i < mResources; i++ ) {
        mResValMap[ i ] = values[ i ];
    }

    repaint();
}

void UsagePlot::paintEvent(QPaintEvent *pEvent)
{
    int w = width();
    int h = height();

    double dw = 1.0 * w / MAX_DATA_POINTS;

    QPainter painter(this);
    painter.setRenderHints(QPainter::Antialiasing);
	painter.fillRect(rect(), Qt::black);

	painter.setPen(Qt::gray);
	painter.drawRect(QRectF(0, 0, w, h));

    for (int r = 0; r < mResources; r++) {
        painter.save();

		painter.setPen(QPen(getColorFor(r), 1));

        /** We'll construct a painter path and then draw the path */
        QPainterPath path;

        /** Move the painter to the first point: p0 */
        path.moveTo( QPointF(dw * 0, h - h * (mResValMap[ r ][ 0 ] - minY) / (maxY - minY)) );

        /** All all points till  */
        for( int i = 1; i < MAX_DATA_POINTS; i++ ) {
            double val1 = ( i < mResValMap[ r ].count() ? mResValMap[ r ][ i ] : 0.0 );
            QPointF p1(dw * i,       h - h * (val1 - minY) / (maxY - minY));

            path.lineTo( p1 );
        }
        painter.drawPath( path );

        painter.restore();
    }

	painter.setPen(QPen(Qt::gray, 2));
	painter.drawLine(0, h, w, h);

    painter.end();

    pEvent->accept();
}

/*
    *
    * UsageGraph - Complete graph with x, y scales and legend
    *
*/

UsageGraph::UsageGraph(const QString& title, const QString& units, int resources, QWidget *parent)
	: QWidget(parent)
	, mTitle(title)
	, mUnits(units)
{
    mResources = resources;

    titleCB = new QToolButton(this);
    titleCB->setText(title + " (" + units + ")");
    titleCB->setCheckable(true);
    titleCB->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);


    titleCB->setChecked(false);
    connect(titleCB, SIGNAL(clicked(bool)), this, SIGNAL(zoomChart()));

    plot = new UsagePlot(resources, this);

    minY = 0;
    maxY = 100;
    plot->setYRange(minY, maxY);

    /* Init y labels */
    double step = ( maxY - minY ) / 4;

    for ( int i = 0; i < 5; i++ ) {
        auto lbl = new QLabel(this);
        lbl->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
        lbl->setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Minimum );
        lbl->setText( QString::number( static_cast<int>( minY + step * i ) ) );

        yLabels.prepend( lbl );
    }

    /* Init legends */
    for (int i = 0; i < resources; i++) {
        auto lbl = new QLabel(this);
        lbl->setAlignment(Qt::AlignCenter);
		lbl->setText(QString("<font color='%1'>Graph %2</font>").arg(getColorFor( i ).name()).arg(i + 1));

        legends << lbl;
    }

	/* Add y labels to layout */
	QVBoxLayout *yLyt = new QVBoxLayout();
	for ( int i = 0; i < 5; i++ ) {
		yLyt->addWidget( yLabels[ i ] );

		if ( i < 4 ) {
			yLyt->addStretch();
		}
	}

	legLyt = new QHBoxLayout();

    for (int i = 0; i < resources; i++) {
		legLyt->addWidget(legends[ i ]);
	}

    QGridLayout *baseLyt = new QGridLayout();
    baseLyt->setSpacing(5);
    baseLyt->addWidget(titleCB, 0, 0, 1, 2);
    baseLyt->addLayout(yLyt, 1, 0);
    baseLyt->addWidget(plot, 1, 1);
    baseLyt->addLayout(legLyt, 2, 0, 1, 2);

    setLayout(baseLyt);
}

void UsageGraph::setYRange(double /* min = 0 */, double max)
{
    maxY = max;

    double fMax = CPrime::FileUtils::formatSizeRaw(static_cast<quint64>(max));
    mUnits = CPrime::FileUtils::formatSizeStr(static_cast<quint64>(max));

    if (mUnits != "%") {
        titleCB->setText(mTitle + " (" + mUnits + ")");
    }

    double step = fMax / 4;

    for ( int i = 0; i < 5; i++ ) {
        yLabels[ i ]->setText( QString::number( minY + step * ( 4 - i ), 'f', 1 ) );
    }

    plot->setYRange(0, max);
}

void UsageGraph::setLegend(const QStringList& lgnds)
{
	// Update the legends as new legend label added
	// Useful when cpu core count changes
	if (legends.count() < lgnds.count()) {
		for (int i = legends.count(); i < lgnds.count(); i++) {
			auto lbl = new QLabel(this);
			lbl->setAlignment(Qt::AlignCenter);

			legends << lbl;
			legLyt->addWidget(legends[ i ]);
		}
	}

	for (int i = 0; i < mResources; i++) {
		legends[ i ]->setText(QString("<font color='%1'><b>%2</b></font>").arg(getColorFor( i ).name(), lgnds.value(i)));
    }

	// Useful when cpu core count changes
	if (lgnds.count() < legends.count()) {
		for (int i = lgnds.count(); i < legends.count(); i++) {
			legends[i]->setText("");
		}
	}
}

void UsageGraph::updateValues(const QList<UsageValues> &values, int resources)
{
	if (mResources < resources) {
		mResources = resources;
	}
	plot->updateValues(values, mResources);
}

bool UsageGraph::zoom()
{
    return titleCB->isChecked();
}

void UsageGraph::resizeEvent( QResizeEvent *rEvent ) {

    rEvent->accept();

    if ( height() < 200 ) {
        yLabels[ 1 ]->hide();
        yLabels[ 3 ]->hide();
    }

    else {
        yLabels[ 1 ]->show();
        yLabels[ 3 ]->show();
    }
}
