/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <QWidget>

/*
    *
    * UsageGraph class
    *
    * UsageGraph is designed to show the current usage of various system resources
    * like CPU, RAM, Swap, Network etc.
    *
    * A single UsageGraph can show multiple data: 4 graphs for quad core CPU,
    * two graphs for Network (Up and Down), and so on...
    *
*/

class QLabel;
class QToolButton;
class QHBoxLayout;

typedef QList<double> UsageValues;

class UsagePlot : public QWidget {
	Q_OBJECT

public:
    UsagePlot(int resources, QWidget *parent);

    /* Y-Range for the graphs */
    void setYRange(double min, double max);

    /* Values for each segment, use absolute values */
	void updateValues(const QList<UsageValues>&, int resources = -1);

private:
    int mResources;
    QHash<int, UsageValues> mResValMap;

    double minY = 0;
    double maxY = 100;

protected:
    void paintEvent(QPaintEvent *) override;
};

class UsageGraph : public QWidget {
	Q_OBJECT

public:
    /* A maximum of 16 resources can be plotted (say for a 16 core processor) */
    UsageGraph(const QString& title, const QString& unit, int resources, QWidget *parent);

    /* Y-Range for the graphs */
    void setYRange(double min, double max);

    /* Legend for the graphs */
    void setLegend(const QStringList&);

    /* Hook to add values to UsagePlot */
	void updateValues(const QList<UsageValues> &, int resources = -1);

    /* To see if the @titbleCB is checked */
    bool zoom();

private:
    int mResources;

    QString mTitle = "Graph";
    QString mUnits = "%";

    double minY = 0;
    double maxY = 100;

    QList<QLabel *> yLabels;
    QList<QLabel *> legends;

    QToolButton *titleCB;

    UsagePlot *plot;
	QHBoxLayout *legLyt;

	static QList<QColor> colors;


protected:
    void resizeEvent( QResizeEvent * ) override;

signals:
    void zoomChart();
};
