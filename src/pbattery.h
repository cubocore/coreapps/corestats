/*
    *
    * This file is a part of CoreStats.
    * A real time system resource viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <QWidget>
#include <QBasicTimer>
#include <csys/battery.h>

namespace Ui {
    class pBattery;
}

class pBattery : public QWidget {
    Q_OBJECT

public:
    explicit pBattery(QWidget *parent = nullptr);
    ~pBattery();

    void reload();

protected:
    void timerEvent(QTimerEvent *event) override;

private slots:
    void on_batteriesList_currentIndexChanged(int index);
    void on_refresh_clicked();

private:
    Ui::pBattery *ui;
    BatteryManager *batman;
    QBasicTimer *timer;
    unsigned int ostate = UINT_MAX;

    void updateList(Battery bat);
    void setupBatteryPage();
};
